import json
import numpy as np
import os
from torchvision import transforms
from PIL import Image
import base64
import io
import torch

global shape_labels
shape_labels = ["beam", "hexagon", "l-profile", "rectangular-tube", "round-bar", "round-tube", "sheet", "square-bar", "t-profile", "u-profile"]

# Called when the service is loaded
def init():
    global shape_labels
    global model
    model_filename = 'shapes_model_densenet.pt'  # Create model name ENV VAR
    # Get the path to the deployed model file and load it
    # Actually create model dir path ENV VAR
    model_path = os.path.join(os.getenv('AZUREML_MODEL_DIR'), model_filename)
    model = torch.load(model_path, map_location='cpu')


# Used for creating prediction of a single image in base64.
def predict(img_base64):
    """
    Predict single image.
    Image must be a valid base64 string.
    """

    # Transform base64 code to an image
    img = base64.b64decode(img_base64)
    buf = io.BytesIO(img)
    img = Image.open(buf).convert('RGB')

    # Create a preprocessing pipeline
    preprocess = transforms.Compose([
        transforms.Resize(256),
        transforms.CenterCrop(224),
        transforms.ToTensor(),
        transforms.Normalize(
            mean=[0.485, 0.456, 0.406],
            std=[0.229, 0.224, 0.225]
        )])

    # Pass the image for preprocessing and the image preprocessed
    img_shape_preprocessed = preprocess(img)

    # Reshape, crop, and normalize the input tensor for feeding into network for evaluation
    batch_img_shape_tensor = img_shape_preprocessed.unsqueeze(0)

    # move the input and model to GPU for speed if available
    if torch.cuda.is_available():
        batch_img_shape_tensor = batch_img_shape_tensor.to('cuda')
        model.to('cuda')

    with torch.no_grad():
        output = model(batch_img_shape_tensor)

    # The output has unnormalized scores. To get probabilities, you can run a softmax on it.
    probabilities = torch.nn.functional.softmax(output[0], dim=0)

    # Predict and get top categories per image
    # For now, only get top 1 category
    top_prob, top_catid = torch.topk(probabilities, 1)

    return top_prob, top_catid


def evaluate_predictions(predictions):
    """
    Evaluate all predicted labels and return the most counted label
    """

    if len(predictions) == 0:
        return None

    # Get predicted labels from all predictions
    predicted_labels = []

    for pred in predictions:
        predicted_labels.append(pred["predicted"])

    counter = 0
    num = []

    # Get most counted labels in predicted_label
    for i in predicted_labels:
        curr_frequency = predicted_labels.count(i)
        if curr_frequency > counter:
            counter = curr_frequency
            num = i

    return num


def evaluate_accuracy_score(evaluated_label, predictions):
    """
    Get evaluated accuracy score of the created predictions.
    """
    if len(predictions) == 0:
        return None
    
    if len(predictions) == 1:
        return predictions[0]['accuracy']

    # Accuracy scores from all predictions that equals evaluated_label
    accuracy_scores_equal_labels = []

    # Accuracy scores from all predictions that differ from the evaluated_label
    accuracy_scores_unequal_labels = []

    for pred in predictions:
        if pred['predicted'] == evaluated_label:
            accuracy_scores_equal_labels.append(pred['accuracy'])
        else:
            accuracy_scores_unequal_labels.append(pred['accuracy'])

    # The accuracy is calculated by combining the sum of the accuracy scores of both equally like predictions and non-like
    # divided by the length of the equally like predictions + 1.
    # The +1 

    length_predictions = len(accuracy_scores_equal_labels) + 1 # Add 1 because of the avg score non-like labels 

    avg_score = (sum(accuracy_scores_equal_labels) + (sum(accuracy_scores_unequal_labels) / len(accuracy_scores_unequal_labels))) / length_predictions

    return avg_score


# Called when a request is received
def run(raw_data):
    # Get the input data as a numpy array
    try:
        data = np.array(json.loads(raw_data))

        predictions = []

        index = 0

        # Iterate through b64 images and predict each image
        for imgb64_item in data:
            # Get a prediction from the model
            top_prob, top_catid = predict(imgb64_item)

            # Append prediction to the predictions list
            predictions.append({
                "index": index,
                "predicted": shape_labels[top_catid[0]],
                "accuracy": top_prob[0].item()
            })

            index += 1

        # Get evaluated shape label
        eval_label = evaluate_predictions(predictions)

        # Get evaluated accuracy score
        eval_accuracy_score = evaluate_accuracy_score(eval_label, predictions)

        if evaluate_predictions is None:
            eval_label = "None"

        output = {
            "predicted": eval_label,
            "accuracy": eval_accuracy_score,
            "predictions": predictions
        }

        # Return the predictions as JSON
        return output
    except ValueError:
        print(ValueError)
        return {"message": "Invalid data/images or server error (check server logs)."}
